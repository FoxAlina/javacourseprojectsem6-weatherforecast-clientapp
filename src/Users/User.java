package Users;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private String userName;
    private String userType;
    private String password;
    private String location;
    private boolean isAccountBlocked;

    public User() {
        this.isAccountBlocked = false;
    }

    public User(String userType) {
        this.userType = userType;
        this.isAccountBlocked = false;
    }

    public User(String userName, String userType, String password, String location) {
        this.userName = userName;
        this.userType = userType;
        this.password = password;
        this.location = location;
        this.isAccountBlocked = false;
    }

    public User(String userName, String userType, String password, String location, boolean setAccountBlocked) {
        this.userName = userName;
        this.userType = userType;
        this.password = password;
        this.location = location;
        this.isAccountBlocked = setAccountBlocked;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isAccountBlocked() {
        return isAccountBlocked;
    }

    public boolean getIsAccountBlocked(){
        return isAccountBlocked;
    }

    public void setAccountBlocked(boolean accountBlocked) {
        isAccountBlocked = accountBlocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return isAccountBlocked == user.isAccountBlocked && userName.equals(user.userName) && userType.equals(user.userType) && password.equals(user.password) && location.equals(user.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, userType, password, location, isAccountBlocked);
    }
}
