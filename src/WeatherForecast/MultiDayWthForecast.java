package WeatherForecast;

import java.io.Serializable;
import java.util.ArrayList;

public class MultiDayWthForecast implements Serializable {
    private int idMultiDayForecast;
    private int idWebsite;
    private String title;
    private String location;
    private ArrayList<WeatherForecast> wthForecast;

    public MultiDayWthForecast() {
        this.wthForecast = new ArrayList<WeatherForecast>();
    }

    public MultiDayWthForecast(int idMultiDayForecast, int idWebsite, String title, String location) {
        this.title = title;
        this.idMultiDayForecast = idMultiDayForecast;
        this.idWebsite = idWebsite;
        this.location = location;
        this.wthForecast = new ArrayList<WeatherForecast>();
    }

    public void setWthForecast(ArrayList<WeatherForecast> wthForecast) {
        this.wthForecast = wthForecast;
    }

    public ArrayList<WeatherForecast> getWthForecast() {
        return wthForecast;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIdMultiDayForecast() {
        return idMultiDayForecast;
    }

    public String getLocation() {
        return location;
    }

    public int getIdWebsite() {
        return idWebsite;
    }

    public String getTitle() {
        return title;
    }

    public void print() {
        System.out.println(this.idMultiDayForecast + " " + this.idWebsite + " " + this.title + " " + this.location);
        for (WeatherForecast t : this.wthForecast) {
            t.print_info();
        }
    }
}
