package WeatherForecast;

import java.io.Serializable;
import java.util.ArrayList;

public class WeatherForecast  implements Serializable {
    private int idForecast;
    private String title;
    private ArrayList<WeatherState> wthStates;

    public WeatherForecast() {
        wthStates = new ArrayList<WeatherState>();
    }

    public WeatherForecast(String title) {
        this.title = title;
        wthStates = new ArrayList<WeatherState>();
    }

    public WeatherForecast(int idForecast, String title) {
        this.idForecast = idForecast;
        this.title = title;
        wthStates = new ArrayList<WeatherState>();
    }

    public void setIdForecast(int idForecast) {
        this.idForecast = idForecast;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setWthStates(ArrayList<WeatherState> wthStates) {
        this.wthStates = wthStates;
    }

    public void setWthStates(WeatherState wthState) {
        this.wthStates.add(wthState);
    }

    public void setWthTimes(String[] times) {
        int i = 0;
        for (WeatherState temp : this.wthStates) {
            temp.setTime(times[i++]);
        }
    }

    public String getTitle() {
        return title;
    }

    public int getIdForecast() {
        return idForecast;
    }

    public void print_info() {
        System.out.println(title);
        for (WeatherState temp : wthStates) {
            temp.print();
        }
    }

    public ArrayList<WeatherState> getWthStates() {
        return wthStates;
    }
}
