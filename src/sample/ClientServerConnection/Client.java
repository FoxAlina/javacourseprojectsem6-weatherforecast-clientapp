package sample.ClientServerConnection;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import Users.User;
import WeatherForecast.MultiDayWthForecast;

public class Client {
    private Socket clientSocket;
    private ObjectOutputStream coos;
    private ObjectInputStream cois;

    public void connectToServer() {
        try {
            this.clientSocket = new Socket("127.0.0.1", 2525);

            this.coos = new ObjectOutputStream(clientSocket.getOutputStream());
            this.cois = new ObjectInputStream(clientSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendToServer(String message) {
        try {
            coos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendToServer(User user) {
        try {
            coos.writeObject(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public <T> T getServerAnswer(T answer) {
        try {
            answer = (T) cois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public void closeConnection() {
        try {
            coos.close();
            cois.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public User logIn(User user) {
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.LOGIN);
        cl.sendToServer(user);
        String ans = "";
        ans = cl.getServerAnswer(ans);
        System.out.println(ans);
        user = cl.getServerAnswer(user);

        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();

        return user;
    }

    public User singUp(User user) {
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.SINGUP);
        cl.sendToServer(user);

        user = cl.getServerAnswer(user);

        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();

        return user;
    }

    public User blockAccount(User user){
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.BLOCK_USER);
        cl.sendToServer(user);
        user = cl.getServerAnswer(user);
        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();
        return user;
    }

    public User unblockAccount(User user){
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.UNBLOCK_USER);
        cl.sendToServer(user);
        user = cl.getServerAnswer(user);
        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();
        return user;
    }

    public User editUserAccount(User oldUser, User newUser){
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.EDIT_ACCOUNT);
        cl.sendToServer(oldUser);
        cl.sendToServer(newUser);
        newUser = cl.getServerAnswer(newUser);
        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();
        return newUser;
    }

    public void deleteUserAccount(User user){
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.DELETE_ACCOUNT);
        cl.sendToServer(user);
        String ans = "";
        ans = cl.getServerAnswer(ans);
        System.out.println(ans);
        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();
    }

    public void updateStoresForecast(){
        //System.out.println("Update");

        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.UPDATE_FORECAST);
        cl.sendToServer(Commands.QUIT);

        String ans = "";
        ans = cl.getServerAnswer(ans);
        //System.out.println(ans);

        cl.closeConnection();
    }

    public ArrayList<MultiDayWthForecast> getForecast(String request) {
        ArrayList<MultiDayWthForecast> forecasts = new ArrayList<MultiDayWthForecast>();
        MultiDayWthForecast forecast = new MultiDayWthForecast();
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.GET_FORECAST);
        cl.sendToServer(request);
        while (forecast != null) {
            forecast = cl.getServerAnswer(forecast);
            if (forecast != null) forecasts.add(forecast);
        }
        //forecasts = cl.getServerAnswer(forecasts);
        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();
        return forecasts;
    }

    public ArrayList<User> getListOfUsers(){
        ArrayList<User> list = new ArrayList<>();
        User user = new User();
        Client cl = new Client();
        cl.connectToServer();
        cl.sendToServer(Commands.GET_USERBASE);
        String ans = "";
        ans = cl.getServerAnswer(ans);
        if(ans.equals(Commands.USER_LIST)){
            /*while(user != null){
                user = cl.getServerAnswer(user);
                if(user != null) list.add(user);
            }*/
            list = cl.getServerAnswer(list);
        }
        cl.sendToServer(Commands.QUIT);
        cl.closeConnection();
        return list;
    }
}
