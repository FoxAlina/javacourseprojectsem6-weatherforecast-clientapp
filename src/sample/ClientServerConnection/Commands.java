package sample.ClientServerConnection;

public class Commands {
    public static final String QUIT = "QUIT";
    public static final String OKEY = "OKEY";
    public static final String YES = "YES";
    public static final String NO = "NO";
    public static final String BLOCKED = "BLOCKED";
    public static final String BLOCK_USER = "BLOCKUSER";
    public static final String UNBLOCK_USER = "UNBLOCKUSER";
    public static final String SET_ROLE = "SETROLE";
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String SINGUP = "SINGUP";
    public static final String LOGIN = "LOGIN";
    public static final String UPDATE_FORECAST = "UPDATEFORECAST";
    public static final String GET_FORECAST = "GETFORECAST";
    public static final String GET_USERBASE = "GETUSERBASE";
    public static final String USER_LIST = "USERLIST";
    public static final String EDIT_ACCOUNT = "EDITACCOUNT";
    public static final String DELETE_ACCOUNT = "DELETEACCOUNT";
}

