package sample.animations;

import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public class PoppingUp {
    private TranslateTransition transTr;

    public PoppingUp(){ }

    public void goForward(Node node){
        this.transTr = new TranslateTransition(Duration.millis(300), node);
        transTr.setFromX(0f);
        transTr.setByX(-405f);
    }

    public void goBack(Node node){
        this.transTr = new TranslateTransition(Duration.millis(300), node);
        transTr.setFromX(0f);
        transTr.setByX(405f);
    }

    public void playAnim(){
        this.transTr.playFromStart();
    }
}
