package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.transform.Translate;
import javafx.scene.layout.VBox;
import javafx.util.Callback;


import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import javafx.scene.chart.CategoryAxis;

import WeatherForecast.WeatherState;
import WeatherForecast.WeatherForecast;
import WeatherForecast.MultiDayWthForecast;
import sample.ClientServerConnection.Client;
import sample.animations.PoppingUp;
import Users.User;


public class AppController extends Controller {
    User user;
    ArrayList<MultiDayWthForecast> multiDayWthForecasts;
    ArrayList<WeatherForecast> chosenForecast;

    int chartPosition;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;


    @FXML
    private Button avargeForecastButton;





    // Client Pane

    @FXML
    private Pane clientPane;

    @FXML
    private Button imageButtonHome;

    @FXML
    private Button imageButtonSettings;

    // Client settings Pane

    @FXML
    private Pane settingsPane;

    @FXML
    private Label userNameLabel;

    @FXML
    private Label userTypeLabel;

    @FXML
    private Label userLocationLabel;

    @FXML
    private Button leaveSettingsButton;

    @FXML
    private Button editAccountPaneButton;

    // Client Settings Edit Account Pane

    @FXML
    private Pane editAccountPane;

    @FXML
    private Button editAccountButton;

    @FXML
    private Button editAccountPaneBackButton;

    @FXML
    private TextField editLoginField;

    @FXML
    private PasswordField editPasswordField;

    @FXML
    private ChoiceBox<String> changeUserLocationChoiceBox;

    //

    //

    @FXML
    private ChoiceBox<String> locationChoiceBox;

    @FXML
    private ChoiceBox<String> websiteChoiceBox;

    @FXML
    private ChoiceBox<String> forecastChoiceBox;

    @FXML
    private Label forecastLabel;

    @FXML
    private Button findForecastButton;

    @FXML
    private Button showForecastButton;

    @FXML
    private Button showGraphButton;

    @FXML
    private Label forecastNameLabel;

    @FXML
    private VBox scrollPaneVBox;

    @FXML
    private Button clearChoiceButton;

    //

    // Admin Pane

    @FXML
    private Pane adminPane;

    @FXML
    private Button adminImageButtonHome;

    @FXML
    private Button adminImageButtonSettings;

    // Admin Settings Pane

    @FXML
    private Pane adminSettingsPane;

    @FXML
    private Label adminUserLocationLabel;

    @FXML
    private Label adminUserNameLabel;

    @FXML
    private Label adminUserTypeLabel;

    @FXML
    private Button adminEditAccountPaneButton;

    @FXML
    private Button adminLeaveSettingsButton;

    // Admin Settings Edit Account Pane

    @FXML
    private Pane adminEditAccountPane;

    @FXML
    private Button adminEditAccountButton;

    @FXML
    private Button adminEditAccountPaneBackButton;

    @FXML
    private TextField adminEditLoginField;

    @FXML
    private PasswordField adminEditPasswordField;

    @FXML
    private ChoiceBox<String> adminChangeUserLocationChoiceBox;

    //

    //

    //

    // Admin Start Pane

    @FXML
    private Pane adminStartPane;

    @FXML
    private Label adminLabel;

    @FXML
    private Label adminWthLabel;

    @FXML
    private Button adminUserBaseButton;

    @FXML
    private Button adminForecastBaseButton;

    //

    // Admin User Pane

    @FXML
    private Pane adminUserPane;

    @FXML
    private Button adminShowUserBase;

    @FXML
    private TableView<User> adminUserTable;

    @FXML
    private Button adminUserPaneBackButton;

    //

    // Admin Weather Forecast Pane

    @FXML
    private Pane adminWthPane;

    @FXML
    private Button adminFindForecastButton;

    @FXML
    private ChoiceBox<String> adminForecastChoiceBox;

    @FXML
    private Label adminChosenForecastNameLabel;

    @FXML
    private Label adminForecastLabel;

    @FXML
    private ChoiceBox<String> adminLocationChoiceBox;

    @FXML
    private VBox adminScrollPaneVBox;

    @FXML
    private Button adminShowForecastButton;

    @FXML
    private Button adminUpdateForecast;

    @FXML
    private Label adminUpdateFrcLabel;

    @FXML
    private ChoiceBox<String> adminWebsiteChoiceBox;

    @FXML
    private Button adminClearChoiceButton;

    @FXML
    private Button adminShowGraphButton;

    @FXML
    private Button adminWthPaneBackButton;

    //

    // Images Wth Conditions

    @FXML
    private ImageView imageClear;

    @FXML
    private ImageView imageCloudy;

    @FXML
    private ImageView imageMainlyCloudy;

    @FXML
    private ImageView imageRainy;

    @FXML
    private ImageView imageSnowy;

    @FXML
    void initialize() {

        if (user.getUserType().equals("Администратор")) {

            clientPane.setVisible(false);
            adminPane.setVisible(true);
            adminStartPane.setVisible(true);
            adminUserPane.setVisible(false);
            adminWthPane.setVisible(false);

            adminEditAccountPane.setVisible(false);
            adminEditAccountPaneButton.setVisible(true);

            adminUpdateFrcLabel.setVisible(false);

            adminUserNameLabel.setText(user.getUserName());
            adminUserTypeLabel.setText(user.getUserType());
            adminUserLocationLabel.setText(user.getLocation());

            adminImageButtonHome.setOnAction(event -> openWind(imageButtonHome, "/sample/view/chooseRole.fxml"));

            adminImageButtonSettings.setOnAction(event -> {
                PoppingUp popUp = new PoppingUp();
                popUp.goForward(adminSettingsPane);
                popUp.playAnim();
            });

            adminLeaveSettingsButton.setOnAction(event -> {
                PoppingUp popUp = new PoppingUp();
                popUp.goBack(adminSettingsPane);
                popUp.playAnim();
            });

            adminForecastBaseButton.setOnAction(event -> {
                adminStartPane.setVisible(false);
                adminWthPane.setVisible(true);
            });

            adminWthPaneBackButton.setOnAction(event -> {
                adminWthPane.setVisible(false);
                adminStartPane.setVisible(true);

                adminUpdateFrcLabel.setVisible(false);
            });

            adminUserBaseButton.setOnAction(event -> {
                adminStartPane.setVisible(false);
                adminUserPane.setVisible(true);
            });

            adminUserPaneBackButton.setOnAction(event -> {
                adminUserPane.setVisible(false);
                adminStartPane.setVisible(true);
            });


            createUserBaseTable();

            adminShowUserBase.setOnAction(event -> showCurrentUserBase());

            adminEditAccountPaneButton.setOnAction(event -> {
                adminEditAccountPaneButton.setVisible(false);
                adminEditAccountPane.setVisible(true);

                String[] locations = {"Минск", "Брест", "Гродно", "Витебск", "Могилёв", "Гомель"};
                adminChangeUserLocationChoiceBox.getItems().addAll(locations);

                adminChangeUserLocationChoiceBox.setValue(this.user.getLocation());
                adminEditLoginField.setText(this.user.getUserName());
                adminEditPasswordField.setText(this.user.getPassword());
            });

            adminEditAccountPaneBackButton.setOnAction(event -> {
                adminEditAccountPane.setVisible(false);
                adminEditAccountPaneButton.setVisible(true);
            });

            adminEditAccountButton.setOnAction(event -> {
                String location = adminChangeUserLocationChoiceBox.getValue();
                String login = adminEditLoginField.getText();
                String password = adminEditPasswordField.getText();
                this.editAccount(location, login, password);
                adminUserNameLabel.setText(user.getUserName());
                adminUserTypeLabel.setText(user.getUserType());
                adminUserLocationLabel.setText(user.getLocation());
            });


            multiDayWthForecasts = new ArrayList<>();

            String[] locations = {"Минск", "Брест", "Гродно", "Витебск", "Могилёв", "Гомель"};
            String[] websites = {"gismeteo.by", "yandex.by", "world-weather.ru"};

            adminLocationChoiceBox.getItems().addAll(locations);
            adminWebsiteChoiceBox.getItems().addAll(websites);

            adminLocationChoiceBox.setValue(user.getLocation());

            this.adminForecastChoiceBox.setVisible(false);
            this.adminShowForecastButton.setVisible(false);
            this.adminShowGraphButton.setVisible(false);
            this.adminForecastLabel.setVisible(false);
            this.adminClearChoiceButton.setVisible(false);

            adminFindForecastButton.setOnAction(event -> {
                setForecastsNames(this.adminForecastChoiceBox, this.adminLocationChoiceBox, this.adminWebsiteChoiceBox);
                this.adminForecastChoiceBox.setVisible(true);
                this.adminShowForecastButton.setVisible(true);
                this.adminForecastLabel.setVisible(true);
                this.adminClearChoiceButton.setVisible(true);
            });

            adminShowForecastButton.setOnAction(event -> {
                showForecast(this.adminScrollPaneVBox,
                        this.adminForecastChoiceBox.getValue(), this.adminChosenForecastNameLabel);
                this.adminShowGraphButton.setVisible(true);
            });

            adminUpdateForecast.setOnAction(event -> {
                this.updateForecastBase();

                adminUpdateFrcLabel.setVisible(true);
            });

            adminClearChoiceButton.setOnAction(event -> {
                this.adminScrollPaneVBox.getChildren().clear();
                this.adminLocationChoiceBox.setValue(this.user.getLocation());
                this.adminWebsiteChoiceBox.setValue("");

                this.adminForecastChoiceBox.setVisible(false);
                this.adminShowForecastButton.setVisible(false);
                this.adminShowGraphButton.setVisible(false);
                this.adminForecastLabel.setVisible(false);
                this.adminClearChoiceButton.setVisible(false);
                this.adminChosenForecastNameLabel.setText("Выбранный прогноз");
            });

            adminShowGraphButton.setOnAction(event -> showGraph(this.adminScrollPaneVBox));

        } else if (user.getUserType().equals("Пользователь")) {

            adminPane.setVisible(false);
            clientPane.setVisible(true);

            editAccountPane.setVisible(false);
            editAccountPaneButton.setVisible(true);

            multiDayWthForecasts = new ArrayList<>();

            userNameLabel.setText(user.getUserName());
            userTypeLabel.setText(user.getUserType());
            userLocationLabel.setText(user.getLocation());

            String[] locations = {"Минск", "Брест", "Гродно", "Витебск", "Могилёв", "Гомель"};
            String[] websites = {"gismeteo.by", "yandex.by", "world-weather.ru"};

            locationChoiceBox.getItems().addAll(locations);
            websiteChoiceBox.getItems().addAll(websites);

            locationChoiceBox.setValue(user.getLocation());

            this.forecastChoiceBox.setVisible(false);
            this.showForecastButton.setVisible(false);
            this.showGraphButton.setVisible(false);
            this.forecastLabel.setVisible(false);
            this.clearChoiceButton.setVisible(false);

            findForecastButton.setOnAction(event -> {
                setForecastsNames(this.forecastChoiceBox, this.locationChoiceBox, this.websiteChoiceBox);
                this.forecastChoiceBox.setVisible(true);
                this.showForecastButton.setVisible(true);
                this.forecastLabel.setVisible(true);
                this.clearChoiceButton.setVisible(true);
            });

            showForecastButton.setOnAction(event -> {
                showForecast(this.scrollPaneVBox, this.forecastChoiceBox.getValue(), this.forecastNameLabel);
                this.showGraphButton.setVisible(true);
            });

            avargeForecastButton.setVisible(false);

            avargeForecastButton.setOnAction(event -> {
                this.getAvargeForecast(this.locationChoiceBox);
                this.showForecast(this.scrollPaneVBox, "Средний прогноз между сайтами", this.forecastNameLabel);
            });

            imageButtonHome.setOnAction(event -> openWind(imageButtonHome, "/sample/view/chooseRole.fxml"));

            imageButtonSettings.setOnAction(event -> {
                PoppingUp popUp = new PoppingUp();
                popUp.goForward(settingsPane);
                popUp.playAnim();
            });

            leaveSettingsButton.setOnAction(event -> {
                PoppingUp popUp = new PoppingUp();
                popUp.goBack(settingsPane);
                popUp.playAnim();
            });

            editAccountPaneButton.setOnAction(event -> {
                editAccountPaneButton.setVisible(false);
                editAccountPane.setVisible(true);

                changeUserLocationChoiceBox.getItems().addAll(locations);

                changeUserLocationChoiceBox.setValue(this.user.getLocation());
                editLoginField.setText(this.user.getUserName());
                editPasswordField.setText(this.user.getPassword());
            });

            editAccountPaneBackButton.setOnAction(event -> {
                editAccountPane.setVisible(false);
                editAccountPaneButton.setVisible(true);
            });

            editAccountButton.setOnAction(event -> {
                String location = changeUserLocationChoiceBox.getValue();
                String login = editLoginField.getText();
                String password = editPasswordField.getText();
                this.editAccount(location, login, password);
                userNameLabel.setText(user.getUserName());
                userTypeLabel.setText(user.getUserType());
                userLocationLabel.setText(user.getLocation());
            });

            clearChoiceButton.setOnAction(event -> {
                this.scrollPaneVBox.getChildren().clear();
                this.locationChoiceBox.setValue(this.user.getLocation());
                this.websiteChoiceBox.setValue("");

                this.forecastChoiceBox.setVisible(false);
                this.showForecastButton.setVisible(false);
                this.showGraphButton.setVisible(false);
                this.forecastLabel.setVisible(false);
                this.clearChoiceButton.setVisible(false);
                this.forecastNameLabel.setText("Выбранный прогноз");
            });

            showGraphButton.setOnAction(event -> showGraph(this.scrollPaneVBox));
        }
    }

    public void initData(User user) {
        this.user = user;
    }

    public void editAccount(String location, String login, String password) {
        User newUser = new User(login, this.user.getUserType(), password, location);
        if (!this.user.equals(newUser)) {
            Client cl = new Client();
            newUser = cl.editUserAccount(this.user, newUser);
            if (newUser != null) this.user = newUser;
        }
    }

    public void deleteAccount(User user) {
        if (user != this.user) {
            Client cl = new Client();
            cl.deleteUserAccount(user);
        }
    }

    public User blockAccount(User user) {
        Client cl = new Client();
        return cl.blockAccount(user);
    }

    public User unblockAccount(User user) {
        Client cl = new Client();
        return cl.unblockAccount(user);
    }

    public void openWind(Button b, String wName) {
        b.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(wName));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        //stage.showAndWait();
        stage.show();
    }

    public void setForecastsNames(ChoiceBox<String> frcChBox, ChoiceBox<String> locChBox, ChoiceBox<String> webChBox) {
        ArrayList<String> forecastsTitles = new ArrayList<>();
        this.multiDayWthForecasts = null;
        this.multiDayWthForecasts = this.getForecasts(locChBox, webChBox);
        if (this.multiDayWthForecasts != null) {
            for (MultiDayWthForecast temp : multiDayWthForecasts) {
                forecastsTitles.add(temp.getTitle());
            }
            frcChBox.getItems().clear();
            frcChBox.getItems().addAll(forecastsTitles);
        }
    }

    public ArrayList<MultiDayWthForecast> getForecasts(ChoiceBox<String> locChBox, ChoiceBox<String> webChBox) {
        if (!locChBox.getValue().equals("") && !webChBox.getValue().equals("")) {
            String request = locChBox.getValue() + "#" + webChBox.getValue();
            Client cl = new Client();
            return cl.getForecast(request);
        }
        return null;
    }


    public void getAvargeForecast(ChoiceBox<String> locChBox) {
        ArrayList<MultiDayWthForecast> gisForecasts = new ArrayList<>();
        ArrayList<MultiDayWthForecast> yanForecasts = new ArrayList<>();
        ArrayList<MultiDayWthForecast> wwForecasts = new ArrayList<>();

        String request = locChBox.getValue() + "#" + "gismeteo.by";
        Client cl = new Client();
        gisForecasts = cl.getForecast(request);
        request = locChBox.getValue() + "#" + "yandex.by";
        yanForecasts = cl.getForecast(request);
        request = locChBox.getValue() + "#" + "world-weather.ru";
        wwForecasts = cl.getForecast(request);

        MultiDayWthForecast gisForecast = new MultiDayWthForecast();
        MultiDayWthForecast yanForecast = new MultiDayWthForecast();
        MultiDayWthForecast wwForecast = new MultiDayWthForecast();

        for (MultiDayWthForecast temp : gisForecasts) {
            String[] s = temp.getTitle().split(" ");
            if (s[s.length - 1].equals("дня")) gisForecast = temp;
        }
        for (MultiDayWthForecast temp : yanForecasts) {
            String[] s = temp.getTitle().split(" ");
            if (s[2].equals("10")) yanForecast = temp;
        }
        for (MultiDayWthForecast temp : wwForecasts) {
            String[] s = temp.getTitle().split(" ");
            if (s[s.length - 1].equals("неделю")) wwForecast = temp;
        }

        ArrayList<WeatherForecast> gisFor = gisForecast.getWthForecast();
        ArrayList<WeatherForecast> yanFor = yanForecast.getWthForecast();
        ArrayList<WeatherForecast> wwFor = wwForecast.getWthForecast();

        MultiDayWthForecast avargeF = new MultiDayWthForecast();
        avargeF.setTitle("Средний прогноз между сайтами");
        avargeF.setLocation(locChBox.getValue());

        ArrayList<WeatherForecast> avargeFor = new ArrayList<>();

        Iterator<WeatherForecast> yanIt = yanFor.iterator();
        Iterator<WeatherForecast> wwIt = wwFor.iterator();
        for (WeatherForecast gisIt : gisFor) {
            ArrayList<WeatherState> yanSts = yanIt.next().getWthStates();
            ArrayList<WeatherState> wwSts = wwIt.next().getWthStates();
            ArrayList<WeatherState> gisSts = gisIt.getWthStates();

            Iterator<WeatherState> yanSt = yanSts.iterator();
            Iterator<WeatherState> wwSt = wwSts.iterator();

            ArrayList<WeatherState> avSts = new ArrayList<>();

            for (WeatherState gisSt : gisSts) {
                String gisT = gisSt.getTemperature().split(" …")[0];
                String[] str = gisT.split("−");
                if (str[0].equals("")) gisT = "-" + str[1];

                String yanT = yanSt.next().getTemperature().split(" …")[0];
                str = yanT.split("−");
                if (str[0].equals("")) yanT = "-" + str[1];

                String wwT = wwSt.next().getTemperature().split(" …")[0];
                str = wwT.split("−");
                if (str[0].equals("")) wwT = "-" + str[1];

                Integer avTemp = Integer.parseInt(gisT) + Integer.parseInt(yanT) + Integer.parseInt(wwT);
                avTemp /= 3;

                String temperature = avTemp.toString();

                avSts.add(new WeatherState(gisSt.getTime(), "н/д",temperature, "н/д",
                        "н/д","н/д","н/д", "н/д", "н/д", "н/д"));
            }
            WeatherForecast wthF = new WeatherForecast(gisIt.getTitle());
            wthF.setWthStates(avSts);
            avargeFor.add(wthF);
        }
        avargeF.setWthForecast(new ArrayList<WeatherForecast>(avargeFor));
        this.multiDayWthForecasts = new ArrayList<MultiDayWthForecast>();
        this.multiDayWthForecasts.add(avargeF);
    }

    public void updateForecastBase() {
        Client cl = new Client();
        cl.updateStoresForecast();
    }

    public void showForecast(VBox vBox, String forecasstName, Label frcNameLabel) {
        int labelY = 5;
        int tableY = 10;
        int i = 0;

        vBox.getChildren().clear();

        String forecastName = forecasstName;
        frcNameLabel.setText(forecastName);
        for (MultiDayWthForecast temp : this.multiDayWthForecasts) {
            if (forecastName.equals(temp.getTitle())) {
                ArrayList<WeatherForecast> wthFors = temp.getWthForecast();
                this.chosenForecast = wthFors;
                for (WeatherForecast fore : wthFors) {

                    Label title = new Label();
                    title.setText(fore.getTitle());

                    Translate translate = new Translate();
                    translate.setX(14);
                    translate.setY(labelY + i);
                    title.getTransforms().addAll(translate);

                    vBox.getChildren().add(title);

                    TableView<WeatherState> table = new TableView<>();
                    table.setPrefWidth(100);
                    table.setPrefHeight(300);

                    TableColumn<WeatherState, String> timeCol = new TableColumn<>("Время");
                    TableColumn<WeatherState, String> wthConCol = new TableColumn<>("Погодные явления");
                    TableColumn<WeatherState, String> tempCol = new TableColumn<>("Температура");
                    TableColumn<WeatherState, String> wSpeedCol = new TableColumn<>("Скорость ветра");
                    TableColumn<WeatherState, String> wDirCol = new TableColumn<>("Направление ветра");
                    TableColumn<WeatherState, String> precipCol = new TableColumn<>("Осадки");
                    TableColumn<WeatherState, String> pressureCol = new TableColumn<>("Давление");
                    TableColumn<WeatherState, String> humidityCol = new TableColumn<>("Влажность");
                    TableColumn<WeatherState, String> feelingCol = new TableColumn<>("Ощущается как");
                    TableColumn<WeatherState, String> probabCol = new TableColumn<>("Вероятность осадков");

                    timeCol.setCellValueFactory(new PropertyValueFactory<>("time"));
                    wthConCol.setCellValueFactory(new PropertyValueFactory<>("wthConditions"));
                    tempCol.setCellValueFactory(new PropertyValueFactory<>("temperature"));
                    wSpeedCol.setCellValueFactory(new PropertyValueFactory<>("windSpeed"));
                    wDirCol.setCellValueFactory(new PropertyValueFactory<>("windDirection"));
                    precipCol.setCellValueFactory(new PropertyValueFactory<>("precipitation"));
                    pressureCol.setCellValueFactory(new PropertyValueFactory<>("pressure"));
                    humidityCol.setCellValueFactory(new PropertyValueFactory<>("humidity"));
                    feelingCol.setCellValueFactory(new PropertyValueFactory<>("feeling"));
                    probabCol.setCellValueFactory(new PropertyValueFactory<>("probability"));

                    ArrayList<WeatherState> wthStates = fore.getWthStates();
                    ObservableList<WeatherState> wthStList = FXCollections.observableArrayList(wthStates);
                    table.setItems(wthStList);


                    TableColumn imageCol = new TableColumn("");
                    imageCol.setCellValueFactory(new PropertyValueFactory<>(""));
                    this.adminUserTable.getColumns().addAll(imageCol);
                    Callback<TableColumn<WeatherState, String>, TableCell<WeatherState, String>> cellFactory =
                            new Callback<TableColumn<WeatherState, String>, TableCell<WeatherState, String>>() {
                                public TableCell call(final TableColumn<WeatherState, String> param) {
                                    final TableCell<WeatherState, String> cell = new TableCell<WeatherState, String>() {

                                        final ImageView imV = new ImageView();

                                        public void updateItem(String item, boolean empty) {
                                            super.updateItem(item, empty);
                                            if (empty) {
                                                setGraphic(null);
                                                setText(null);
                                            } else {
                                                String wthCond;
                                                WeatherState wS = getTableView().getItems().get(getIndex());
                                                String[] str = wS.getWthConditions().split(",");
                                                if (str.length == 1) str = wS.getWthConditions().split(" ");
                                                wthCond = str[0];

                                                if (wthCond.equals("Пасмурно")) {
                                                    imV.setImage(imageMainlyCloudy.getImage());
                                                } else if (wthCond.equals("Ясно")) {
                                                    imV.setImage(imageClear.getImage());
                                                } else if (wthCond.equals("Облачно")) {
                                                    imV.setImage(imageCloudy.getImage());
                                                } else if (wthCond.equals("Дождь")) {
                                                    imV.setImage(imageRainy.getImage());
                                                } else if (wthCond.equals("Снег")) {
                                                    imV.setImage(imageSnowy.getImage());
                                                }

                                                imV.setFitHeight(50);
                                                imV.setFitWidth(50);
                                                setGraphic(imV);
                                                setText(null);
                                            }
                                        }
                                    };
                                    return cell;
                                }
                            };
                    imageCol.setCellFactory(cellFactory);


                    table.getColumns().addAll(timeCol, wthConCol, imageCol, tempCol, wSpeedCol, wDirCol, precipCol,
                            pressureCol, humidityCol, feelingCol, probabCol);

                    translate = new Translate();
                    translate.setX(7);
                    translate.setY(tableY + i);
                    table.getTransforms().addAll(translate);

                    vBox.getChildren().add(table);

                    i += 30;
                }
            }
        }
        chartPosition = i + 10;
    }

    public void showGraph(VBox scVBox) {

        ObservableList<XYChart.Data> temperatureTime = FXCollections.observableArrayList();

        if (this.chosenForecast != null) {
            for (WeatherForecast fore : this.chosenForecast) {
                ArrayList<WeatherState> wthSt = fore.getWthStates();
                for (WeatherState st : wthSt) {
                    String s = st.getTemperature().split(" …")[0];
                    String[] str = s.split("−");
                    if (str[0].equals("")) s = "-" + str[1];
                    temperatureTime.add(new XYChart.Data(fore.getTitle() + " " + st.getTime(), Integer.parseInt(s)));
                }
            }
        }

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Время");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Температура");

        XYChart.Series series1 = new XYChart.Series();
        series1.setData(temperatureTime);

        LineChart linechart = new LineChart(xAxis, yAxis);
        linechart.setTitle("График температуры");
        linechart.getData().addAll(series1);

        Translate translate = new Translate();
        translate.setY(chartPosition);
        linechart.getTransforms().addAll(translate);

        scVBox.getChildren().add(linechart);
    }

    public ArrayList<User> getUserBase() {
        Client cl = new Client();
        return cl.getListOfUsers();
    }

    public void createUserBaseTable() {
        TableColumn<User, String> userName;
        TableColumn<User, String> userType;
        TableColumn<User, String> userLocation;
        TableColumn<User, Boolean> isBlocked;

        userName = new TableColumn<>("Логин");
        userType = new TableColumn<>("Роль");
        userLocation = new TableColumn<>("Город");
        isBlocked = new TableColumn<>("Заблокирован ли");

        userName.setCellValueFactory(new PropertyValueFactory<>("userName"));
        userType.setCellValueFactory(new PropertyValueFactory<>("userType"));
        userLocation.setCellValueFactory(new PropertyValueFactory<>("location"));
        isBlocked.setCellValueFactory(new PropertyValueFactory<>("isAccountBlocked"));

        this.adminUserTable.getColumns().addAll(userName, userType, userLocation, isBlocked);


        TableColumn blockButtonCol = new TableColumn("Заблокировать/разблокировать");
        blockButtonCol.setCellValueFactory(new PropertyValueFactory<>(""));
        this.adminUserTable.getColumns().addAll(blockButtonCol);
        Callback<TableColumn<User, String>, TableCell<User, String>> cellFactory =
                new Callback<TableColumn<User, String>, TableCell<User, String>>() {
                    public TableCell call(final TableColumn<User, String> param) {
                        final TableCell<User, String> cell = new TableCell<User, String>() {

                            final Button btn = new Button("Button");

                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    User user = getTableView().getItems().get(getIndex());
                                    if (user.isAccountBlocked()) btn.setText("Разблокировать");
                                    else btn.setText("Заблокировать");

                                    btn.setOnAction(event -> {
                                        if (!user.isAccountBlocked()) {
                                            blockAccount(user);
                                            showCurrentUserBase();
                                        } else {
                                            unblockAccount(user);
                                            showCurrentUserBase();
                                        }
                                        System.out.println(btn.getText() + " " + user.getUserType() + "   " + user.getUserName());
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        blockButtonCol.setCellFactory(cellFactory);


        TableColumn deleteButtonCol = new TableColumn("Удалить");
        this.adminUserTable.getColumns().addAll(deleteButtonCol);
        Callback<TableColumn<User, String>, TableCell<User, String>> deleteCellFactory =
                new Callback<TableColumn<User, String>, TableCell<User, String>>() {
                    public TableCell call(final TableColumn<User, String> param) {
                        final TableCell<User, String> cell = new TableCell<User, String>() {

                            final Button btn = new Button("Удалить");

                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction(event -> {
                                        User user = getTableView().getItems().get(getIndex());
                                        deleteAccount(user);
                                        showCurrentUserBase();
                                        System.out.println(btn.getText() + " " + user.getUserType() + "   " + user.getUserName());
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        deleteButtonCol.setCellFactory(deleteCellFactory);
    }

    public void showCurrentUserBase() {
        ArrayList<User> list = this.getUserBase();
        this.adminUserTable.getItems().clear();
        ObservableList<User> userList = FXCollections.observableArrayList(list);
        this.adminUserTable.setItems(userList);
    }
}

