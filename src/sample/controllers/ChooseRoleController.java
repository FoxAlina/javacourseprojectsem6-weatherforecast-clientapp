package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import Users.User;

public class ChooseRoleController extends Controller{

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button adminButton;

    @FXML
    private Button userButton;

    @FXML
    void initialize() {
        /*Client client = new Client();
        client.connectToServer();
        client.sendToServer("HELLO");
        client.closeConnection();*/

        adminButton.setOnAction(event -> {
            User user = new User("Администратор");
            LogInController controller = new LogInController();
            controller.initData(user);
            openWind(controller, adminButton, "/sample/view/logIn.fxml");
        });

        userButton.setOnAction(event -> {
            User user = new User("Пользователь");
            LogInController controller = new LogInController();
            controller.initData(user);
            openWind(controller, userButton, "/sample/view/logIn.fxml");
        });
    }

    public void openWind(Controller controller, Button b, String wName){
        b.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(wName));

        loader.setController(controller);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        //stage.showAndWait();
        stage.show();
    }
}
