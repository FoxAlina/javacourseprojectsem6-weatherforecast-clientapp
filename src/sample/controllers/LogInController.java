package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import sample.ClientServerConnection.Client;
import Users.User;

public class LogInController extends Controller{
    private User user;


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button logInButton;

    @FXML
    private TextField loginField;

    @FXML
    private Button singUpButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Label wrongInput;

    @FXML
    private Label wrongInput1;

    @FXML
    private Button imageButtonHome;

    @FXML
    private Label roleLabel;

    @FXML
    void initialize() {
        wrongInput.setVisible(false);
        wrongInput1.setVisible(false);

        roleLabel.setText(user.getUserType());


        logInButton.setOnAction(actionEvent -> {
            final User u = this.loginUser();
            if(u != null) {
                this.user = u;
                AppController controller = new AppController();
                controller.initData(user);
                openWind(controller, logInButton, "/sample/view/app.fxml");
                wrongInput.setVisible(false);
            }
        });

        singUpButton.setOnAction(actionEvent -> {
            SingUpController controller = new SingUpController();
            controller.initData(user);
            openWind(controller, singUpButton, "/sample/view/singUp.fxml");
        });

        imageButtonHome.setOnAction(event -> {
            openWind(imageButtonHome, "/sample/view/chooseRole.fxml");
        });
    }

    public void initData(User user){
     this.user = user;
    }

    private User loginUser() {
        User u = this.user;
        String login = loginField.getText().trim();
        String password = passwordField.getText().trim();
        int pass = password.hashCode();
        if (!login.equals("") && !password.equals("")) {
            u.setUserName(login);
            u.setPassword(password);

            Client cl = new Client();
            u = cl.logIn(u);
            if(u != null){
                if(!u.isAccountBlocked()) this.user = u;
                else if(u.isAccountBlocked()) {
                    u = null;
                    wrongInput.setVisible(false);
                    wrongInput1.setVisible(true);
                }
            }else {
                wrongInput1.setVisible(false);
                wrongInput.setVisible(true);
            }
            return u;
        } else return null;
    }

    public void openWind(Button b, String wName){
        b.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(wName));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }

    public void openWind(Controller controller, Button b, String wName){
        b.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(wName));

        loader.setController(controller);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        //stage.showAndWait();
        stage.show();
    }

}

