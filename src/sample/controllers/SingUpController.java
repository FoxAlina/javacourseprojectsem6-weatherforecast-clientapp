package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import Users.User;
import sample.ClientServerConnection.Client;

public class SingUpController extends Controller {
    private User user;


    @FXML
    private Button logInButton;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ChoiceBox<String> locationChoiceBox;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button singUpButton;

    @FXML
    private Label emptyFields;

    @FXML
    private Label userExists;

    @FXML
    private Button imageButtonHome;

    @FXML
    private Label roleLabel;

    @FXML
    void initialize() {
        emptyFields.setVisible(false);
        userExists.setVisible(false);

        String[] locations = {"Минск", "Брест", "Гродно", "Витебск", "Могилёв", "Гомель"};
        locationChoiceBox.getItems().addAll(locations);
        locationChoiceBox.setValue("Минск");

        roleLabel.setText(user.getUserType());

        singUpButton.setOnAction(event -> {
            if (!loginField.equals("") && !passwordField.equals("")) {
                final User u = signUpUser();
                if (u != null) {
                    this.user = u;
                    AppController controller = new AppController();
                    controller.initData(user);
                    openWind(controller, singUpButton, "/sample/view/app.fxml");
                    userExists.setVisible(false);
                } else {
                    userExists.setVisible(true);
                }
            } else emptyFields.setVisible(true);
        });

        logInButton.setOnAction(event -> {
            LogInController controller = new LogInController();
            controller.initData(user);
            openWind(controller, logInButton, "/sample/view/logIn.fxml");
        });

        imageButtonHome.setOnAction(event -> {
            openWind(imageButtonHome, "/sample/view/chooseRole.fxml");
        });
    }

    public void initData(User user) {
        this.user = user;
    }

    private User signUpUser() {
        User u = this.user;

        u.setUserName(loginField.getText());
        u.setPassword(passwordField.getText());
        u.setLocation(locationChoiceBox.getValue());

        Client cl = new Client();
        u = cl.singUp(u);
        if (u != null) this.user = u;
        return u;
    }

    public void openWind(Button b, String wName) {
        b.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(wName));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        //stage.showAndWait();
        stage.show();
    }

    public void openWind(Controller controller, Button b, String wName) {
        b.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(wName));

        loader.setController(controller);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        //stage.showAndWait();
        stage.show();
    }
}
